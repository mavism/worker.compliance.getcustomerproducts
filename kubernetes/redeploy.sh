#!/usr/bin/env bash

function echoBold () {
    echo $'\n\e[1m'"${1}"$'\e[0m'
}

echoBold 'Deleting Camunda Worker...'
kubectl delete -f kubernetes-deployment.yaml

echoBold 'Deploying Camunda Worker...'
kubectl create -f kubernetes-deployment.yaml