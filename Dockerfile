FROM node:8-alpine

COPY package.json package-lock.json ./

RUN npm set progress=false && npm config set depth 0 && npm cache clean --force

RUN mkdir /ng-app

## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
WORKDIR /ng-app

COPY . .

RUN npm rebuild node-sass

RUN npm i

RUN npm install gulp -g

CMD ["npm", "start"]
