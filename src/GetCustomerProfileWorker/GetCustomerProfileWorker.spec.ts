//During the test the env variable is set to test
import GetCustomerProfileWorker from "./GetCustomerProfileWorker";
import axios from "axios";
import {Customer} from "../models/Customer";
import {Identification} from "../models/Identification";
import {NationalityCodes} from "../models/NationalityCodes";
import {CustomerProfile} from "../models/CustomerProfile";

process.env.NODE_ENV = 'test';
process.env.KUBE_API_URL = 'https://kube-develop.nvizible.co.za';

//Require the dev-dependencies
// let chaiObj = require('chai');
// let should = chaiObj.should();

//Our parent block
describe('Service worker', () => {
    /*
      * Test the /GET route
      */
    describe('Basic Worker', () => {
        it('it should be up and running', async (done) => {
            // let worker = new GetCustomerProfileWorker();
            // workerCheck(context:any, callback:any) {
            //     console.log('Camunda response....', context);

            // let response: basicResponse;
            let hgid = 'hg1021';

            // Get the Customer profile
            if (typeof hgid !== 'undefined') {

                try {
                    console.log("customer start: ", hgid);
                    const customerPromise = axios(process.env.KUBE_API_URL + "/hgProfile/customers/" + hgid);
                    const identityPromise = axios(process.env.KUBE_API_URL + '/hgProfile/identifications?hgid=' + hgid);
                    const productPromise = axios(process.env.KUBE_API_URL + '/hp-core/api/products?hgid=' + hgid);
                    const [customerResponse, identificationResponse, productResponse] = await Promise.all([customerPromise, identityPromise, productPromise]);

                    let products = productResponse.data.data;
                    let customer: Customer = customerResponse.data;
                    let identification: Identification = identificationResponse.data.data[0];

                    const nationalityPromise = axios(process.env.KUBE_API_URL + '/hp-core/api/nationalityCodes/' + customer.CountryofBirth);
                    const [nationalityResponse] = await Promise.all([nationalityPromise]);

                    let nationalityCode: NationalityCodes = nationalityResponse.data;

                    let customerProfile: CustomerProfile = new CustomerProfile();
                    customerProfile.firstName = customer.FirstName;
                    customerProfile.surname = customer.Surname;
                    customerProfile.dateOfBirth = customer.Dob;
                    customerProfile.countryOfBirth = nationalityCode.code;
                    customerProfile.idType = customer.IdType;
                    customerProfile.idNumber = customer.IdNumber;
                    customerProfile.creationDate = customer.CreationDate;
                    customerProfile.nationality = customer.Nationality;
                    customerProfile.channel = customer.Channel;
                    customerProfile.idCountryOfIssue = identification.CountryOfIssue;
                    customerProfile.mrzNumber = typeof identification.MrzNumber != undefined ? identification.MrzNumber : "null";
                    customerProfile.occupationalIndustry = customer.OccupationalIndustry;
                    customerProfile.occupationalStatus = customer.OccupationalStatus;

                    //product related
                    customerProfile.hpProduct = false;
                    customerProfile.bankProduct = false;
                    products.forEach(product => {
                        switch (product.HelloProductsId) {
                            case 1: //bank
                                customerProfile.bankProduct = true;
                                break;
                            case 2: //hp
                                customerProfile.hpProduct = true;
                                break;
                            default:
                                console.log("default switch");
                        }
                    });

                    //what to Do with these??
                    customerProfile.identityReviewRequired = true;
                    customerProfile.proofOfAddressReviewRequired = true;
                    customerProfile.sourceOfFundsRequired = true;
                    customerProfile.personImageReviewRequired = true;
                    customerProfile.identityReviewRequired = true; //if we have ID data ... and .. ?
                    customerProfile.termsAndConditionsRequired = true;
                    customerProfile.additionalDocumentsRequired = true;
                    customerProfile.fatcaReviewRequired = true; //does have Tax info
                    customerProfile.sanctionsScreeningRequired = false;
                    customerProfile.adverseMediaRequired = true; //toDo: Should always see this - even without current results?

                    //still toDo
                    customerProfile.adverseMediaStatus = false; //toDo: check if any adverse media exist for this HGid
                    customerProfile.hpCat4AccountStatus = "null";

                    console.log("Response: ", customerProfile);
                    // callback(null, {
                    //     variables: customerProfile
                    // });
                } catch (error) {
                    console.log("error", error);
                    if (error.statusCode == 404) {
                        console.log("Not found:", error.statusCode);
                        // callback(null, {
                        //     errorCode: 'No Record Found'
                        // });
                    }
                    console.log("AXIOS ERROR status : ", error.response.status);
                    console.log("AXIOS ERROR: ", error);
                    // callback(
                    //     new Error(error.toString())
                    // )
                }
            } else {
                console.log("invalid HgId");
                // callback(null, {
                //     errorCode: 'invalid hgid'
                // });
            }
        })
    })
});