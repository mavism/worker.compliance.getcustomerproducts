import * as Worker from 'camunda-worker-node';
import * as Backoff from 'camunda-worker-node/lib/backoff';
import axios from "axios";
import {Customer} from "../models/Customer";
import {Identification} from "../models/Identification";
import {CustomerProfile} from "../models/CustomerProfile";
import {NationalityCodes} from "../models/NationalityCodes";

export class GetCustomerProfileWorker {

    private engineEndpoint = process.env.ENGINE_URL || '';
    public workerObj = Worker(this.engineEndpoint, {
        workerId: 'WORKER.GetCustomerProfile',
        use: [
            Backoff
        ]
    });

    /**
     * Initialize the VerifyWorker
     */
    constructor() {
        console.log("constructed GetCustomerProfileWorker");
        this.workerObj.subscribe('WORKER.Compliance.GetCustomerProfile', ['hgid'], this.workerCheck);
    }

    async workerCheck(context: any, callback: any) {
        console.log('Camunda response....', context);

        // let response: basicResponse;
        let hgid = context.variables.hgid;

        // Get the Customer profile
        if (typeof hgid !== 'undefined') {

            try {
                console.log("customer start: ", hgid);
                const customerPromise = axios(process.env.KUBE_API_URL + "/hgProfile/customers/" + hgid);
                const identityPromise = axios(process.env.KUBE_API_URL + '/hgProfile/identifications?hgid=' + hgid);
                const productPromise = axios(process.env.KUBE_API_URL + '/hp-core/api/products?hgid=' + hgid);
                const adverseMediaPromise = axios(process.env.KUBE_API_URL + '/hgProfile/adverseMedia?hgid=' + hgid);
                const [customerResponse, identificationResponse, productResponse, adverseMediaResponse] = await Promise.all([customerPromise, identityPromise, productPromise, adverseMediaPromise]);

                let products = productResponse.data.data;
                let customer: Customer = customerResponse.data;
                let identification: Identification = identificationResponse.data.data[0];

                const nationalityPromise = axios(process.env.KUBE_API_URL + '/hp-core/api/nationalityCodes/' + customer.CountryofBirth);
                const [nationalityResponse] = await Promise.all([nationalityPromise]);

                let nationalityCode: NationalityCodes = nationalityResponse.data;

                let customerProfile: CustomerProfile = new CustomerProfile();

                //customer Information
                customerProfile.firstName = customer.FirstName;
                customerProfile.surname = customer.Surname;
                customerProfile.dateOfBirth = customer.Dob;
                customerProfile.countryOfBirth = nationalityCode.code;
                customerProfile.idType = customer.IdType;
                customerProfile.idNumber = customer.IdNumber;
                customerProfile.creationDate = customer.CreationDate;
                customerProfile.nationality = customer.Nationality;
                customerProfile.channel = customer.Channel;
                customerProfile.idCountryOfIssue = identification.CountryOfIssue;
                customerProfile.mrzNumber = typeof identification.MrzNumber != undefined ? identification.MrzNumber : "null";
                customerProfile.occupationalIndustry = customer.OccupationalIndustry;
                customerProfile.occupationalStatus = customer.OccupationalStatus;

                //product related
                customerProfile.hpProduct = false;
                customerProfile.bankProduct = false;
                products.forEach(product => {
                    switch (product.HelloProductsId) {
                        case 1: //bank
                            customerProfile.bankProduct = true;
                            break;
                        case 2: //hp
                            customerProfile.hpProduct = true;
                            break;
                        default:
                            console.log("default switch");
                    }
                });

                //tab reviews
                customerProfile.identityReviewRequired = true;
                customerProfile.proofOfAddressReviewRequired = true;
                customerProfile.sourceOfFundsRequired = true;
                customerProfile.personImageReviewRequired = true;
                customerProfile.identityReviewRequired = true;
                customerProfile.termsAndConditionsRequired = true;
                customerProfile.additionalDocumentsRequired = true;
                customerProfile.fatcaReviewRequired = true;
                customerProfile.sanctionsScreeningRequired = false;
                customerProfile.adverseMediaRequired = true;

                //Statuses
                if (adverseMediaResponse.data.data.length > 0) {
                    let trip = false;
                    adverseMediaResponse.data.data.forEach(function(item) {
                        if (item.AdverseMediaTypeID != 8)
                            trip = true;
                    });
                    customerProfile.adverseMediaStatus = trip;
                } else {
                    customerProfile.adverseMediaStatus = false;
                }

                if (customerProfile.hpProduct) {
                    try {
                        const limitsResponse = await axios(process.env.KUBE_API_URL + '/limits/customers/' + hgid + '/limits');
                        if (limitsResponse.data.accountStatus.includes('CAT')) { //toDo: Should remove this IF check after flow updates: HB-759 HB-760
                            customerProfile.hpCat4AccountStatus = limitsResponse.data.accountStatus;
                        }
                    } catch (error) {
                        customerProfile.hpCat4AccountStatus = null;
                    }
                } else {
                    customerProfile.hpCat4AccountStatus = null;
                }

                console.log("Response: ", customerProfile);
                callback(null, {
                    variables: customerProfile
                });
            } catch (error) {
                console.log("error", error);
                if (error.statusCode == 404) {
                    console.log("Not found:", error.statusCode);
                    // callback(null, {
                    //     errorCode: 'No Record Found'
                    // });
                }
              if (typeof error.response !== 'undefined') {
                console.log("AXIOS ERROR status : ", error.response.status);
              }
                console.log("AXIOS ERROR: ", error);
                // callback(
                //     new Error(error.toString())
                // )
            }
        } else {
            callback(null, {
                errorCode: 'invalid hgid'
            });
        }
    }
}

export default GetCustomerProfileWorker;
