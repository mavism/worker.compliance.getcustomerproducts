export class NationalityCodes {

    private _id: number;
    private _code: string;
    private _nationalityDescr: string;
    private _isDisabled: string;
    private _isSelfKYC: string;
    private _Updated: string;
    private _Iso2: string;

    constructor(id: number, code: string, nationalityDescr: string, isDisabled: string, isSelfKYC: string, Updated: string, Iso2: string) {
        this._id = id;
        this._code = code;
        this._nationalityDescr = nationalityDescr;
        this._isDisabled = isDisabled;
        this._isSelfKYC = isSelfKYC;
        this._Updated = Updated;
        this._Iso2 = Iso2;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get code(): string {
        return this._code;
    }

    set code(value: string) {
        this._code = value;
    }

    get nationalityDescr(): string {
        return this._nationalityDescr;
    }

    set nationalityDescr(value: string) {
        this._nationalityDescr = value;
    }

    get isDisabled(): string {
        return this._isDisabled;
    }

    set isDisabled(value: string) {
        this._isDisabled = value;
    }

    get isSelfKYC(): string {
        return this._isSelfKYC;
    }

    set isSelfKYC(value: string) {
        this._isSelfKYC = value;
    }

    get Updated(): string {
        return this._Updated;
    }

    set Updated(value: string) {
        this._Updated = value;
    }

    get Iso2(): string {
        return this._Iso2;
    }

    set Iso2(value: string) {
        this._Iso2 = value;
    }
}