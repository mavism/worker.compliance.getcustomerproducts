export class Customer{

    private _HgId;
    private _Title;
    private _Gender;
    private _FirstName;
    private _Surname;
    private _Msisdn;
    private _IdType;
    private _IdNumber;
    private _Nationality;
    private _PassportExpiryDate;
    private _ForeignIDNumber;
    private _PermitNumber;
    private _PermitExpiryDate;
    private _Dob;
    private _Province;
    private _City;
    private _Suburb;
    private _BuildingNo;
    private _Complex;
    private _StreetNo;
    private _StreetName;
    private _PostalCode;
    private _Latitude;
    private _Longitude;
    private _ProofOfAddrType;
    private _ProofOfIncomeType;
    private _Email;
    private _DealerCode;
    private _CreationDate;
    private _UpdatedDate;
    private _HgAgentId;
    private _FicaExpiryDate;
    private _FicaExpiryNotice;
    private _StatusId;
    private _FicaYesNo;
    private _Captured_Id_Images;
    private _Captured_Proof_Image;
    private _ChineseApproved;
    private _LoyaltyOptInId;
    private _LoyaltyOptInDate;
    private _SecondaryMsisdn;
    private _Channel;
    private _CountryofBirth;
    private _Captured_Selfie_Image;
    private _SOFprovided;
    private _fkHelloEmploymentTypeID;
    private _id;
    private _HelloCustomerID;
    private _AlternateMSISDN;
    private _IsStaff;
    private _StaffNumber;
    private _MaritalStatus;
    private _EmployerName;
    private _OccupationalStatus;
    private _OccupationalIndustry;
    private _AllowMarketing;
    private _AllowMarketingByEmail;
    private _AllowMarketingBySMS;
    private _AllowMarketingByMail;
    private _AllowNotificationByEmail;
    private _AllowNotificationBySMS;
    private _ContactComment;
    private _ComplianceStateID;
    private _CreatedAt;
    private _UpdatedAt;
    private _ResidencyNationalityCode: string;
    private _IsDeceased: boolean;

    constructor(HgId, Title, Gender, FirstName, Surname, Msisdn, IdType, IdNumber, Nationality, PassportExpiryDate, ForeignIDNumber, PermitNumber, PermitExpiryDate, Dob, Province, City, Suburb, BuildingNo, Complex, StreetNo, StreetName, PostalCode, Latitude, Longitude, ProofOfAddrType, ProofOfIncomeType, Email, DealerCode, CreationDate, UpdatedDate, HgAgentId, FicaExpiryDate, FicaExpiryNotice, StatusId, FicaYesNo, Captured_Id_Images, Captured_Proof_Image, ChineseApproved, LoyaltyOptInId, LoyaltyOptInDate, SecondaryMsisdn, Channel, CountryofBirth, Captured_Selfie_Image, SOFprovided, fkHelloEmploymentTypeID, id, HelloCustomerID, AlternateMSISDN, IsStaff, StaffNumber, MaritalStatus, EmployerName, OccupationalStatus, OccupationalIndustry, AllowMarketing, AllowMarketingByEmail, AllowMarketingBySMS, AllowMarketingByMail, AllowNotificationByEmail, AllowNotificationBySMS, ContactComment, ComplianceStateID, CreatedAt, UpdatedAt, ResidencyNationalityCode: string, IsDeceased: boolean) {
        this._HgId = HgId;
        this._Title = Title;
        this._Gender = Gender;
        this._FirstName = FirstName;
        this._Surname = Surname;
        this._Msisdn = Msisdn;
        this._IdType = IdType;
        this._IdNumber = IdNumber;
        this._Nationality = Nationality;
        this._PassportExpiryDate = PassportExpiryDate;
        this._ForeignIDNumber = ForeignIDNumber;
        this._PermitNumber = PermitNumber;
        this._PermitExpiryDate = PermitExpiryDate;
        this._Dob = Dob;
        this._Province = Province;
        this._City = City;
        this._Suburb = Suburb;
        this._BuildingNo = BuildingNo;
        this._Complex = Complex;
        this._StreetNo = StreetNo;
        this._StreetName = StreetName;
        this._PostalCode = PostalCode;
        this._Latitude = Latitude;
        this._Longitude = Longitude;
        this._ProofOfAddrType = ProofOfAddrType;
        this._ProofOfIncomeType = ProofOfIncomeType;
        this._Email = Email;
        this._DealerCode = DealerCode;
        this._CreationDate = CreationDate;
        this._UpdatedDate = UpdatedDate;
        this._HgAgentId = HgAgentId;
        this._FicaExpiryDate = FicaExpiryDate;
        this._FicaExpiryNotice = FicaExpiryNotice;
        this._StatusId = StatusId;
        this._FicaYesNo = FicaYesNo;
        this._Captured_Id_Images = Captured_Id_Images;
        this._Captured_Proof_Image = Captured_Proof_Image;
        this._ChineseApproved = ChineseApproved;
        this._LoyaltyOptInId = LoyaltyOptInId;
        this._LoyaltyOptInDate = LoyaltyOptInDate;
        this._SecondaryMsisdn = SecondaryMsisdn;
        this._Channel = Channel;
        this._CountryofBirth = CountryofBirth;
        this._Captured_Selfie_Image = Captured_Selfie_Image;
        this._SOFprovided = SOFprovided;
        this._fkHelloEmploymentTypeID = fkHelloEmploymentTypeID;
        this._id = id;
        this._HelloCustomerID = HelloCustomerID;
        this._AlternateMSISDN = AlternateMSISDN;
        this._IsStaff = IsStaff;
        this._StaffNumber = StaffNumber;
        this._MaritalStatus = MaritalStatus;
        this._EmployerName = EmployerName;
        this._OccupationalStatus = OccupationalStatus;
        this._OccupationalIndustry = OccupationalIndustry;
        this._AllowMarketing = AllowMarketing;
        this._AllowMarketingByEmail = AllowMarketingByEmail;
        this._AllowMarketingBySMS = AllowMarketingBySMS;
        this._AllowMarketingByMail = AllowMarketingByMail;
        this._AllowNotificationByEmail = AllowNotificationByEmail;
        this._AllowNotificationBySMS = AllowNotificationBySMS;
        this._ContactComment = ContactComment;
        this._ComplianceStateID = ComplianceStateID;
        this._CreatedAt = CreatedAt;
        this._UpdatedAt = UpdatedAt;
        this._ResidencyNationalityCode = ResidencyNationalityCode;
        this._IsDeceased = IsDeceased;
    }

    get HgId() {
        return this._HgId;
    }

    set HgId(value) {
        this._HgId = value;
    }

    get Title() {
        return this._Title;
    }

    set Title(value) {
        this._Title = value;
    }

    get Gender() {
        return this._Gender;
    }

    set Gender(value) {
        this._Gender = value;
    }

    get FirstName() {
        return this._FirstName;
    }

    set FirstName(value) {
        this._FirstName = value;
    }

    get Surname() {
        return this._Surname;
    }

    set Surname(value) {
        this._Surname = value;
    }

    get Msisdn() {
        return this._Msisdn;
    }

    set Msisdn(value) {
        this._Msisdn = value;
    }

    get IdType() {
        return this._IdType;
    }

    set IdType(value) {
        this._IdType = value;
    }

    get IdNumber() {
        return this._IdNumber;
    }

    set IdNumber(value) {
        this._IdNumber = value;
    }

    get Nationality() {
        return this._Nationality;
    }

    set Nationality(value) {
        this._Nationality = value;
    }

    get PassportExpiryDate() {
        return this._PassportExpiryDate;
    }

    set PassportExpiryDate(value) {
        this._PassportExpiryDate = value;
    }

    get ForeignIDNumber() {
        return this._ForeignIDNumber;
    }

    set ForeignIDNumber(value) {
        this._ForeignIDNumber = value;
    }

    get PermitNumber() {
        return this._PermitNumber;
    }

    set PermitNumber(value) {
        this._PermitNumber = value;
    }

    get PermitExpiryDate() {
        return this._PermitExpiryDate;
    }

    set PermitExpiryDate(value) {
        this._PermitExpiryDate = value;
    }

    get Dob() {
        return this._Dob;
    }

    set Dob(value) {
        this._Dob = value;
    }

    get Province() {
        return this._Province;
    }

    set Province(value) {
        this._Province = value;
    }

    get City() {
        return this._City;
    }

    set City(value) {
        this._City = value;
    }

    get Suburb() {
        return this._Suburb;
    }

    set Suburb(value) {
        this._Suburb = value;
    }

    get BuildingNo() {
        return this._BuildingNo;
    }

    set BuildingNo(value) {
        this._BuildingNo = value;
    }

    get Complex() {
        return this._Complex;
    }

    set Complex(value) {
        this._Complex = value;
    }

    get StreetNo() {
        return this._StreetNo;
    }

    set StreetNo(value) {
        this._StreetNo = value;
    }

    get StreetName() {
        return this._StreetName;
    }

    set StreetName(value) {
        this._StreetName = value;
    }

    get PostalCode() {
        return this._PostalCode;
    }

    set PostalCode(value) {
        this._PostalCode = value;
    }

    get Latitude() {
        return this._Latitude;
    }

    set Latitude(value) {
        this._Latitude = value;
    }

    get Longitude() {
        return this._Longitude;
    }

    set Longitude(value) {
        this._Longitude = value;
    }

    get ProofOfAddrType() {
        return this._ProofOfAddrType;
    }

    set ProofOfAddrType(value) {
        this._ProofOfAddrType = value;
    }

    get ProofOfIncomeType() {
        return this._ProofOfIncomeType;
    }

    set ProofOfIncomeType(value) {
        this._ProofOfIncomeType = value;
    }

    get Email() {
        return this._Email;
    }

    set Email(value) {
        this._Email = value;
    }

    get DealerCode() {
        return this._DealerCode;
    }

    set DealerCode(value) {
        this._DealerCode = value;
    }

    get CreationDate() {
        return this._CreationDate;
    }

    set CreationDate(value) {
        this._CreationDate = value;
    }

    get UpdatedDate() {
        return this._UpdatedDate;
    }

    set UpdatedDate(value) {
        this._UpdatedDate = value;
    }

    get HgAgentId() {
        return this._HgAgentId;
    }

    set HgAgentId(value) {
        this._HgAgentId = value;
    }

    get FicaExpiryDate() {
        return this._FicaExpiryDate;
    }

    set FicaExpiryDate(value) {
        this._FicaExpiryDate = value;
    }

    get FicaExpiryNotice() {
        return this._FicaExpiryNotice;
    }

    set FicaExpiryNotice(value) {
        this._FicaExpiryNotice = value;
    }

    get StatusId() {
        return this._StatusId;
    }

    set StatusId(value) {
        this._StatusId = value;
    }

    get FicaYesNo() {
        return this._FicaYesNo;
    }

    set FicaYesNo(value) {
        this._FicaYesNo = value;
    }

    get Captured_Id_Images() {
        return this._Captured_Id_Images;
    }

    set Captured_Id_Images(value) {
        this._Captured_Id_Images = value;
    }

    get Captured_Proof_Image() {
        return this._Captured_Proof_Image;
    }

    set Captured_Proof_Image(value) {
        this._Captured_Proof_Image = value;
    }

    get ChineseApproved() {
        return this._ChineseApproved;
    }

    set ChineseApproved(value) {
        this._ChineseApproved = value;
    }

    get LoyaltyOptInId() {
        return this._LoyaltyOptInId;
    }

    set LoyaltyOptInId(value) {
        this._LoyaltyOptInId = value;
    }

    get LoyaltyOptInDate() {
        return this._LoyaltyOptInDate;
    }

    set LoyaltyOptInDate(value) {
        this._LoyaltyOptInDate = value;
    }

    get SecondaryMsisdn() {
        return this._SecondaryMsisdn;
    }

    set SecondaryMsisdn(value) {
        this._SecondaryMsisdn = value;
    }

    get Channel() {
        return this._Channel;
    }

    set Channel(value) {
        this._Channel = value;
    }

    get CountryofBirth() {
        return this._CountryofBirth;
    }

    set CountryofBirth(value) {
        this._CountryofBirth = value;
    }

    get Captured_Selfie_Image() {
        return this._Captured_Selfie_Image;
    }

    set Captured_Selfie_Image(value) {
        this._Captured_Selfie_Image = value;
    }

    get SOFprovided() {
        return this._SOFprovided;
    }

    set SOFprovided(value) {
        this._SOFprovided = value;
    }

    get fkHelloEmploymentTypeID() {
        return this._fkHelloEmploymentTypeID;
    }

    set fkHelloEmploymentTypeID(value) {
        this._fkHelloEmploymentTypeID = value;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get HelloCustomerID() {
        return this._HelloCustomerID;
    }

    set HelloCustomerID(value) {
        this._HelloCustomerID = value;
    }

    get AlternateMSISDN() {
        return this._AlternateMSISDN;
    }

    set AlternateMSISDN(value) {
        this._AlternateMSISDN = value;
    }

    get IsStaff() {
        return this._IsStaff;
    }

    set IsStaff(value) {
        this._IsStaff = value;
    }

    get StaffNumber() {
        return this._StaffNumber;
    }

    set StaffNumber(value) {
        this._StaffNumber = value;
    }

    get MaritalStatus() {
        return this._MaritalStatus;
    }

    set MaritalStatus(value) {
        this._MaritalStatus = value;
    }

    get EmployerName() {
        return this._EmployerName;
    }

    set EmployerName(value) {
        this._EmployerName = value;
    }

    get OccupationalStatus() {
        return this._OccupationalStatus;
    }

    set OccupationalStatus(value) {
        this._OccupationalStatus = value;
    }

    get OccupationalIndustry() {
        return this._OccupationalIndustry;
    }

    set OccupationalIndustry(value) {
        this._OccupationalIndustry = value;
    }

    get AllowMarketing() {
        return this._AllowMarketing;
    }

    set AllowMarketing(value) {
        this._AllowMarketing = value;
    }

    get AllowMarketingByEmail() {
        return this._AllowMarketingByEmail;
    }

    set AllowMarketingByEmail(value) {
        this._AllowMarketingByEmail = value;
    }

    get AllowMarketingBySMS() {
        return this._AllowMarketingBySMS;
    }

    set AllowMarketingBySMS(value) {
        this._AllowMarketingBySMS = value;
    }

    get AllowMarketingByMail() {
        return this._AllowMarketingByMail;
    }

    set AllowMarketingByMail(value) {
        this._AllowMarketingByMail = value;
    }

    get AllowNotificationByEmail() {
        return this._AllowNotificationByEmail;
    }

    set AllowNotificationByEmail(value) {
        this._AllowNotificationByEmail = value;
    }

    get AllowNotificationBySMS() {
        return this._AllowNotificationBySMS;
    }

    set AllowNotificationBySMS(value) {
        this._AllowNotificationBySMS = value;
    }

    get ContactComment() {
        return this._ContactComment;
    }

    set ContactComment(value) {
        this._ContactComment = value;
    }

    get ComplianceStateID() {
        return this._ComplianceStateID;
    }

    set ComplianceStateID(value) {
        this._ComplianceStateID = value;
    }

    get CreatedAt() {
        return this._CreatedAt;
    }

    set CreatedAt(value) {
        this._CreatedAt = value;
    }

    get UpdatedAt() {
        return this._UpdatedAt;
    }

    set UpdatedAt(value) {
        this._UpdatedAt = value;
    }

    get ResidencyNationalityCode(): string {
        return this._ResidencyNationalityCode;
    }

    set ResidencyNationalityCode(value: string) {
        this._ResidencyNationalityCode = value;
    }

    get IsDeceased(): boolean {
        return this._IsDeceased;
    }

    set IsDeceased(value: boolean) {
        this._IsDeceased = value;
    }
}