export class Identification{

    private _Id: string;
    private _HelloCustomerID: string;
    private _HelloIDType: string;
    private _IdNumber: string;
    private _MrzNumber: string;
    private _CountryOfIssue: string;
    private _IssueDate: string;
    private _ExpiryDate: string;
    private _FicaVerified: string;
    private _IsActive: string;
    private _CreatedAt: string;
    private _UpdatedAt: string;

    constructor(Id, HelloCustomerID, HelloIDType, IdNumber, MrzNumber, CountryOfIssue, IssueDate, ExpiryDate, FicaVerified, IsActive, CreatedAt, UpdatedAt) {
        this._Id = Id;
        this._HelloCustomerID = HelloCustomerID;
        this._HelloIDType = HelloIDType;
        this._IdNumber = IdNumber;
        this._MrzNumber = MrzNumber;
        this._CountryOfIssue = CountryOfIssue;
        this._IssueDate = IssueDate;
        this._ExpiryDate = ExpiryDate;
        this._FicaVerified = FicaVerified;
        this._IsActive = IsActive;
        this._CreatedAt = CreatedAt;
        this._UpdatedAt = UpdatedAt;
    }

    get Id() {
        return this._Id;
    }

    set Id(value) {
        this._Id = value;
    }

    get HelloCustomerID() {
        return this._HelloCustomerID;
    }

    set HelloCustomerID(value) {
        this._HelloCustomerID = value;
    }

    get HelloIDType() {
        return this._HelloIDType;
    }

    set HelloIDType(value) {
        this._HelloIDType = value;
    }

    get IdNumber() {
        return this._IdNumber;
    }

    set IdNumber(value) {
        this._IdNumber = value;
    }

    get MrzNumber() {
        return this._MrzNumber;
    }

    set MrzNumber(value) {
        this._MrzNumber = value;
    }

    get CountryOfIssue() {
        return this._CountryOfIssue;
    }

    set CountryOfIssue(value) {
        this._CountryOfIssue = value;
    }

    get IssueDate() {
        return this._IssueDate;
    }

    set IssueDate(value) {
        this._IssueDate = value;
    }

    get ExpiryDate() {
        return this._ExpiryDate;
    }

    set ExpiryDate(value) {
        this._ExpiryDate = value;
    }

    get FicaVerified() {
        return this._FicaVerified;
    }

    set FicaVerified(value) {
        this._FicaVerified = value;
    }

    get IsActive() {
        return this._IsActive;
    }

    set IsActive(value) {
        this._IsActive = value;
    }

    get CreatedAt() {
        return this._CreatedAt;
    }

    set CreatedAt(value) {
        this._CreatedAt = value;
    }

    get UpdatedAt() {
        return this._UpdatedAt;
    }

    set UpdatedAt(value) {
        this._UpdatedAt = value;
    }
}