export class CustomerProfile {

    public channel: number | undefined;
    public hgid: string | number | undefined;
    public idDocumentId: number | undefined;
    public idType: number | undefined;
    public idNumber: string | undefined;
    public idFicaVerified: boolean | undefined;
    public idIsActive: boolean | undefined;
    public idCreatedAt: string | undefined;
    public idUpdatedAt: string | undefined;
    public mrzNumber: string | null | undefined;
    public firstName: string | undefined;
    public surname: string | undefined;
    public initials: string | undefined;
    public dateOfBirth: string | undefined;
    public permitNumber: string | undefined;
    public asylumNumber: string | undefined;
    public idCountryOfIssue: string | undefined;
    public idIssueDate: string | undefined;
    public idExpiryDate: string | undefined;
    public passportExpiryDate: string | undefined;
    public permitExpiryDate: string | undefined;
    public asylumExpiryDate: string | undefined;
    public countryOfBirth: string | undefined;
    public idImageID: string | undefined;
    public permitImageID: string | undefined;
    public nationality: string | undefined;
    public gender: string | undefined;
    public maritalStatus: string | undefined;
    public deceaseStatus: string | undefined;
    public msisdn: string | undefined;
    public alternateMSISDN: string | undefined;
    public emailAddress: string | undefined;
    public statusID: string | undefined;
    public productName: string | undefined;
    public personImageID: string | undefined;

    public selectToCaptureProofOfAddress: string | undefined;
    public proofOfAddressDocumentType: string | undefined;
    public selectedProofOfAddressMethod: string | undefined;
    public proofOfAddressDocumentImageID: string | undefined;
    public postalAddressId: number | undefined;
    public postalAddressPostBoxOrStreetNumber: string | undefined;
    public postalAddressPostBoxTypeOrStreetName: string | undefined;
    public postalAddressComplexName: string | undefined;
    public postalAddressComplexNumber: string | undefined;
    public postalAddressSuburb: string | undefined;
    public postalAddressCity: string | undefined;
    public postalAddressProvince: string | undefined;
    public postalAddressCountry: string | undefined;
    public postalAddressPostalCode: string | undefined;
    public postalAddressCreatedAt: string | undefined;
    public postalAddressUpdatedAt: string | undefined;
    public physicalAddressId: number | undefined;
    public physicalAddressStreetNumber: number | undefined;
    public physicalAddressStreetname: string | undefined;
    public physicalAddressComplexName: string | undefined;
    public physicalAddressComplexNumber: string | undefined;
    public physicalAddressSuburb: string | undefined;
    public physicalAddressCity: string | undefined;
    public physicalAddressProvince: string | undefined;
    public physicalAddressCountry: string | undefined;
    public physicalAddressPostalCode: string | undefined;
    public physicalAddressCreatedAt: string | undefined;
    public physicalAddressUpdatedAt: string | undefined;

    public sourceOfFundsId: string | undefined;
    public sourceOfFundsType: string | undefined;
    public mainIncomeSource: string | undefined;
    public grossMonthlyIncome: string | undefined;
    public grossMonthlyIncomeCode: string | undefined;
    public frequencyOfIncome: string | undefined;
    public proofOfIncomeType: string | undefined;
    public positionHeld: string | undefined;
    public occupationalIndustry: string | undefined;
    public occupationalStatus: string | undefined;
    public nameOfEmployer: string | undefined;
    public sourceOfFundsMainCategory: string | undefined;
    public salaryPaidIntoAccount: string | undefined;
    public frequencyofIncome: string | undefined;
    public grossMonthlyIncomeCategory: string | undefined;

    public adverseMediaId: number | undefined;
    public adverseMediaFound: boolean | undefined;
    public adverseMediaSource: string | undefined;
    public adverseMediaDescription: string | undefined;
    public adverseMediaUrl: string | undefined;
    public adverseMediaImageId: number | undefined;
    public adverseMediaReporter: string | undefined;

    public usTaxObligationId: number | undefined;
    public usTaxObligation: boolean | undefined;
    public usTaxIdentificationNumber: number | undefined;
    public taxObligationId: number | undefined;
    public otherCountriesOfTaxObligation: boolean | undefined;
    public taxIdentificationNumber: string | undefined;
    public countryOfTaxObligation: string | undefined;
    public taxReferenceNumber: string | undefined;
    public hpProduct: boolean | undefined;
    public bankProduct: boolean | undefined;
    public bankRiskScore: string | undefined;
    public bankRiskRatingCategory: string | undefined;
    public hpRiskScore: string | undefined;
    public hpRiskRatingCategory: string | undefined;
    public hbid: string | undefined;
    public hpid: string | undefined;

    public creationDate: string | undefined;
    public updatedDate: string | undefined;
    public hgAgentId: string | undefined;
    public Channel: string | undefined;
    public FICAYesNo: string | undefined;

    public identityReviewRequired: boolean | undefined;
    public personImageReviewRequired: boolean | undefined;
    public proofOfAddressReviewRequired: boolean | undefined;
    public sourceOfFundsRequired: boolean | undefined;
    public fatcaReviewRequired: boolean | undefined;
    public termsAndConditionsRequired: boolean | undefined;
    public adverseMediaRequired: boolean | undefined;
    public additionalDocumentsRequired: boolean | undefined;
    public sanctionsScreeningRequired: boolean | undefined;

    public adverseMediaStatus: boolean | undefined;
    public hpCat4AccountStatus: string | boolean | null | undefined;
}