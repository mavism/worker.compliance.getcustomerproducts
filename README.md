
# TODO Replace these keywords

WORKER.GetCustomerProfile

`This is the Unique name of your worker`

worker-camunda-get-customer-profile                    

`This is the name of your worker project`

WORKER.Compliance.GetCustomerProfile       

`This is the unique topic name that your worker listens to`

hgid                   

`This is how variables are passed in from camunda`

http://wso2apim.nvizible.co.za:8243/cms/v1/customers/{hgid}                              

`This is your internal API url`

res.data.hgid                 

`This is example of success response with error message handler`

getCustomerProfileError              

`This is how you throw a errorCode for BPMN in camunda`

camunda-worker-compliance

`This is the kubernetes namespace. Change according to your worker type.`

## BASE NODE WORKER
This project was forked from the base node worker project